package sn.atos.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
@WebServlet(name = "continent", value = "/continent")
public class ContinentServelt extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        System.out.print("***** Cassandra - Java Connection Tester ******");
        Cluster cluster;
        Session session;
        // Connect to the cluster and keyspace "lab_cassandra" 127.0.0.1:9042/9160
        cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        session = cluster.connect("lab_cassandra");

        // Use insert statement

        // Use select statement
        String iso_code = null, continent = null, location = null;
        float  total_cases = 0;


        // Use select statement
        ResultSet results = session.execute(
                "Select iso_code,continent,total_cases,new_cases,total_deaths,new_deaths from covid where date >= '2021-01-01' group by continent,iso_code ALLOW FILTERING;");
        req.setAttribute("lists",results);

     /*   for (Row row : results) {
            iso_code = row.getString("iso_code");
            continent = row.getString("continent");
           // location = row.getString("location");
           // total_cases = row.getFloat(" total_cases");


            System.out.println("iso_code: " + iso_code);
            System.out.println("continent: " + continent);
           // System.out.println("location: " + location);
            System.out.println("total_cases: "+ total_cases);
        }*/
        System.out.println("closing");
        // Clean up the connection by closing it
        getServletContext().getRequestDispatcher("/continent.jsp").forward(req, res);

        cluster.close();



    }
}
