package sn.atos.controller;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Scanner;

public class main {
    public static void main(String[] args){


      System.out.print("***** Cassandra - Java Connection Tester ******");
    Cluster cluster;
    Session session;
    // Connect to the cluster and keyspace "lab_cassandra" 127.0.0.1:9042/9160
    cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
    session = cluster.connect("lab_cassandra");

    // Use insert statement

    // Use select statement
    String iso_code = null, continent = null, location = null;
    BigDecimal total_cases,total_deaths,new_cases = BigDecimal.valueOf(0);

        Scanner sc = new Scanner(System.in);
        System.out.println("Saisir la date sur cette format : 2020-01-01");

        String d=  sc.next();
    // Use select statement
    ResultSet results = session.execute(
            "Select iso_code,continent,total_cases,new_cases,total_deaths,new_deaths from covid where date = '"+d.toString()+"' group by continent,iso_code ALLOW FILTERING;");

        for (Row row : results) {
            iso_code = row.getString("iso_code");
            continent = row.getString("continent");
            total_cases = row.getDecimal("total_cases");
            new_cases = row.getDecimal("new_cases");
            total_deaths = row.getDecimal("total_deaths");


            System.out.println("iso_code: " + iso_code);
            System.out.println("continent: " + continent);
            System.out.println("total_cases: "+ total_cases);
            System.out.println("new_cases: "+ new_cases);
            System.out.println("total_deaths: "+ total_deaths);
        }
        System.out.println("closing");
    // Clean up the connection by closing it

        cluster.close();
    }
}
