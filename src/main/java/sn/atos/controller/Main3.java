package sn.atos.controller;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.math.BigDecimal;
import java.util.Scanner;

public class Main3 {
    public static void main(String[] args){


        System.out.print("***** Cassandra - Java Connection Tester ******");
        Cluster cluster;
        Session session;
        // Connect to the cluster and keyspace "lab_cassandra" 127.0.0.1:9042/9160
        cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        session = cluster.connect("lab_cassandra");

        // Use insert statement

        // Use select statement
        String iso_code = null, continent = null;

        // Use select statement
        ResultSet results = session.execute(
                "select distinct continent ,iso_code  from covid  allow filtering;");
        for (Row row : results) {
            iso_code = row.getString("iso_code");
            continent = row.getString("continent");

            System.out.println("iso_code: " + iso_code);
            System.out.println("continent: " + continent);

        }
        System.out.println("closing");
        // Clean up the connection by closing it

        cluster.close();
    }
}
