<%@ page  language="java" contentType="text/html; charset=UTF-8"  %>


<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
<div class="row g-3 align-items-center">
    <div class="col-auto">
        <label for="date" class="col-form-label">date</label>
    </div>
    <div class="col-auto">
        <input type="date" id="date" class="form-control" name="date">
    </div>
    <div class="col-auto">
    <span id="passwordHelpInline" class="form-text">
     <button type="submit" class="btn btn-primary">Submit</button>
    </span>
    </div>
</div>
<br>
<table class="table">
    <thead>
    <tr>
        <th scope="col">continent </th>
        <th scope="col">total cases </th>
        <th scope="col">new cases </th>
        <th scope="col">total deaths </th>
        <th scope="col">new deaths  </th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${lists}" var="list">
        <tr>
            <td>${list.continent }</td>
            <td>${list.total_cases  }</td>
            <td>${list.new_cases  }</td>
            <td>${list.total_deaths  }</td>
            <td>${list.new_deaths }</td>

        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>